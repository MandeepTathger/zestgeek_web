import React from "react"
import { Container, Row, Col } from 'reactstrap';
import Heads from './Heads.js'
import './Ideas.css'

const Ideas = () =>(
	<Container fluid >
		<Heads text="Great Ideas Start <br />at Zestgeek" borderColor='#f2f2f2' bgColor='#fff' />
		<Row className="marginTop">
			<Col md={{size: 5, offset:1, order:2}} text-right>
				<img src="images/right..png" />
			</Col>
			<Col md={{size: 6, order:1}} className="center" text-left>
				<div className="ideaContent left">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					<button>Read More</button>
				</div>
			</Col>
		</Row>
		<Row className="marginBottom">
			<Col md="5">
				<img src="images/left.png" />
			</Col>
			<Col md={{size: 6, offset:1}} className="center">
				<div className="ideaContent right">
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
					<button>Read More</button>
				</div>
			</Col>
		</Row>
	</Container>
)

export default Ideas;