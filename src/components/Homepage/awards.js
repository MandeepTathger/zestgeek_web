import React from 'react'
import './awards.css'
import { Container, Col, Row} from 'reactstrap';


const Awards = () =>(
	<Container fluid className="awards">
		<Row>
			<Col md="5" className="heading">
				<h1>Awards & Accolades</h1>
			</Col>
			<Col md="7" className="awardImg">
				<img className="awardImg1" src="images/guru.png"/>
				<img className="awardImg2" src="images/upwork.png"/>
				<img className="awardImg3" src="images/goodfirm-1.png"/>
				<img className="awardImg4" src="images/software-development-1.png"/>
			</Col>
		</Row>
	</Container>
)

export default Awards