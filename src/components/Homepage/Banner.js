import React from "react"
import './Banner.css'

const Banner = () =>(
	<div className="backImg" id="home">
		<div className="bannerText">
			<h1>Lorem Ipsum is simply dummy text of the printing and type setting industry</h1>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
			<button>Read More</button>
		</div>
	</div>
)

export default Banner