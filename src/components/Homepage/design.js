import React from "react"
import "./design.css"
import { Container, Row, Col } from 'reactstrap';

const Design = () =>(
	<Container fluid className="steps" id="steps">
		<Row>
			<Col md="6">
				<div className="stepImg">
					<img src="images/andrew.png"/>
				</div>
			</Col>
			<Col md="6">
				<Row>
					<Col md={{ size: 11, offset: 1 }} className="stepCon">
						<div className="stepItem">
							<i className="stepIcon speaking"></i>
							<div className="padLeft">
								<h1>Let's Talk</h1>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. There are many variations of passages of Lorem Ipsum available.</p>							
							</div>
						</div>
						<div className="stepItem">
							<i className="stepIcon technology"></i>
							<div className="padLeft">
								<h1>Design</h1>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. There are many variations of passages of Lorem Ipsum available.</p>					
							</div>
						</div>
						<div className="stepItem">
							<i className="stepIcon prog"></i>
							<div className="padLeft">
								<h1>Development</h1>
								<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. There are many variations of passages of Lorem Ipsum available.</p>					
							</div>
						</div>
					</Col>
				</Row>
			</Col>
		</Row>
	</Container>
)

export default Design