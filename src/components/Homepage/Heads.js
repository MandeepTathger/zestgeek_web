import React from "react"
import './Heads.css';

const Heads = (props) =>(
	<div className="headMargin">
		<div className="headsCenter">
			<h1 dangerouslySetInnerHTML={{__html: props.text}} style={{backgroundColor: props.bgColor}}></h1>
			<div className="after" style={{borderColor: props.borderColor}}></div>
		</div>
	</div>
)

export default Heads;


/*const Heads = ({text, bgColor}) =>(
	<div className="headsCenter">
		<h1 dangerouslySetInnerHTML={{__html: text}} style={{backgroundColor: bgColor}}></h1>
	</div>
)

export default Heads;*/