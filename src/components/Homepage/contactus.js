import React from 'react'
import './contactus.css'
import { Container, Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
import FA from 'react-fontawesome'

const Contact = () =>(
	<Container className="contactUs" id="contact">
		<h1>Get in Touch</h1>
		<Form className="contactForm">
	      <Row form>
	        <Col md={6}>
	          <FormGroup>
	          	<i className=" iconCommon formIcon1"></i>
	            <Input type="text" name="name" id="exampleEmail" placeholder="Your Name" />
	          </FormGroup>
	        </Col>
	        <Col md={6}>
	          <FormGroup>
	          	<i className=" iconCommon formIcon2"></i>
	            <Input type="email" name="email" id="examplePassword" placeholder="Email" />
	          </FormGroup>
	        </Col>
	      </Row>
	      <FormGroup>
	      	<i className=" iconCommon formIcon3"></i>
	        <Input type="textarea" name="message" id="exampleAddress" placeholder="Message..."/>
	      </FormGroup>
	      <Button>Submit</Button>
	    </Form>
	</Container>
)

export default Contact