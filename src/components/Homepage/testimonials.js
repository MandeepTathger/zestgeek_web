import React from "react"
import Heads from './Heads.js'
import './testimonials.css'
import { Container, Row, Col} from 'reactstrap';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import Rating from 'react-rating'

const options = {
    responsiveClass: true,
    nav: true,
    autoplay: true,
    items: 2,
    responsive: {
    	0: {
            items: 1,
        },
        400: {
            items: 1,
        },
        600: {
            items: 1,
        },
        700: {
            items: 1,
        },
        1000: {
            items: 2,

        }
    },
};

const Testimonial = () =>(
	<Container fluid className="testimonial" id="blog">
		<Heads text="Testimonials" borderColor='#fff' bgColor='#f7f7f7' />
		<div className="sliderCon">
			<OwlCarousel className="owl-theme" {...options}>
		    <div className="item">
		    	<div className="itemContent">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>						
		    		<div className="user">
		    			<div className="userProfile">
		    				<div className="userImg">
		    					<img src="images/profile_user.png"/>
		    				</div>
		    				<div className="userDetail">
		    					<h6>John Deo</h6>
		    					<p>Company CEO</p>
		    				</div>
		    			</div>
		    			<div className="rating">
			    			<Rating
									emptySymbol="fa fa-star-o fa-2x"
									fullSymbol="fa fa-star fa-2x"
									initialRating={5}
									readonly
								/>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <div className="item">
		    	<div className="itemContent">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>						
		    		<div className="user">
		    			<div className="userProfile">
		    				<div className="userImg">
		    					<img src="images/profile_user.png"/>
		    				</div>
		    				<div className="userDetail">
		    					<h6>John Deo</h6>
		    					<p>Company CEO</p>
		    				</div>
		    			</div>
		    			<div className="rating">
			    			<Rating
									emptySymbol="fa fa-star-o fa-2x"
									fullSymbol="fa fa-star fa-2x"
									initialRating={5}
									readonly
								/>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <div className="item">
		    	<div className="itemContent">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>						
		    		<div className="user">
		    			<div className="userProfile">
		    				<div className="userImg">
		    					<img src="images/profile_user.png"/>
		    				</div>
		    				<div className="userDetail">
		    					<h6>John Deo</h6>
		    					<p>Company CEO</p>
		    				</div>
		    			</div>
		    			<div className="rating">
			    			<Rating
									emptySymbol="fa fa-star-o fa-2x"
									fullSymbol="fa fa-star fa-2x"
									initialRating={5}
									readonly
								/>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		    <div className="item">
		    	<div className="itemContent">
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>						
		    		<div className="user">
		    			<div className="userProfile">
		    				<div className="userImg">
		    					<img src="images/profile_user.png"/>
		    				</div>
		    				<div className="userDetail">
		    					<h6>John Deo</h6>
		    					<p>Company CEO</p>
		    				</div>
		    			</div>
		    			<div className="rating">
			    			<Rating
									emptySymbol="fa fa-star-o fa-2x"
									fullSymbol="fa fa-star fa-2x"
									initialRating={5}
									readonly
								/>
							</div>
		    		</div>
		    	</div>
		    </div>
			</OwlCarousel>
		</div>
	</Container>
)

export default Testimonial