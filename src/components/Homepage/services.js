import React from "react"
import { Container, Row, Col } from 'reactstrap';
import './service.css'
import Heads from './Heads.js'
import { graphql } from "gatsby"

const Service = (props) =>{
	console.log(props, 'props')
	return (
		<Container fluid className="services" id="service">
			<Heads text="Our Service" borderColor='#fff' bgColor='#f7f7f7' />
			<div className="content">
				<Row>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg1"></i>
							<h4>UI & UX Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</Col>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg2"></i>
							<h4>React JS Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</Col>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg3"></i>
							<h4>PHP Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</Col>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg4"></i>
							<h4>Mobile App Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</Col>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg5"></i>
							<h4>Digital Marketing</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>
					</Col>
					<Col md="4" className="boxes">
						<div className="bgColor">
							<i className="icon iconimg6"></i>
							<h4>Rest API Development</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
						</div>				
					</Col>
				</Row>
			</div>
		</Container>
	)
}

export default Service



export const query = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
  }
`