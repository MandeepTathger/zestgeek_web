import PropTypes from "prop-types"
import React from "react"
import { Container, Row, Col } from 'reactstrap';
import './Footer.css'
import FA from 'react-fontawesome'

const Footer = () => (
  <footer>
		<Container fluid>
			<Row className="textColor">
				<Col md="4">
					<div>
						<h3>About us</h3>
						<p>The easiest and the fastest way to create a React app with server-side rendering.</p>
					</div>
				</Col>
				<Col md="3" className="links">
					<h3>Quick Links</h3>
					<ul>
						<li><a href="">Home</a></li>
						<li><a href="">Our Service</a></li>
						<li><a href="">Carrier</a></li>
						<li><a href="">Our Team</a></li>
						<li><a href="">Blog</a></li>
						<li><a href="">Contact Us</a></li>
					</ul>
				</Col>
				<Col md="3" className="links">
					<h3>Services</h3>
					<ul>
						<li><a href="">UI & UX Development</a></li>
						<li><a href="">React Development</a></li>
						<li><a href="">PHP Development</a></li>
						<li><a href="">Mobile App Development</a></li>
						<li><a href="">Contact Us</a></li>
						<li><a href="">Node.JS Development</a></li>
					</ul>
				</Col>
				<Col md="2" className="boxIcons">
					<h3>Contact Us</h3>
					<a href=""><FA name="facebook" /></a>
					<a href=""><FA name="twitter" /></a>
					<a href=""><FA name="linkedin" /></a>
					<a href=""><FA name="instagram" /></a>
				</Col>
			</Row>
		</Container>
  </footer>
)

Footer.propTypes = {
  siteTitle: PropTypes.string,
}

Footer.defaultProps = {
  siteTitle: ``,
}

export default Footer
