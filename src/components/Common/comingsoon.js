import React from 'react'
import {Container}from 'reactstrap'
import './comingsoon.css'

const Comingsoon = () =>(
	<Container fluid className="comingSoon">
		<div className="box">
			<h1>Coming Soon</h1>
			<p>We are currently working on this feature and will launch soon</p>
		</div>
	</Container>
)

export default Comingsoon