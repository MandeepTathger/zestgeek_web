import PropTypes from "prop-types"
import React, { useState } from "react"
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';
import Image from "../image.js";
import './CommonNavbar.css'
import FA from 'react-fontawesome'
import ScrollIntoView from 'react-scroll-into-view'
import { Link } from "gatsby"

const CommonNavbar = () => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="cNavbar">
      <Navbar color="transparent" expand="md" dark>
        <NavbarBrand href="/" className="logo"> 
          <img src="images/logo1.png" />
        </NavbarBrand>
        <NavbarToggler onClick={toggle}/>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            <a className="removeIcon" href="javascript:void(0)" onClick={toggle}><FA name="angle-right" /></a>
            <Link to="/">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Home</NavLink>
              </NavItem>
            </Link>
            <ScrollIntoView selector="#service">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Our Service</NavLink>
              </NavItem>
            </ScrollIntoView>
            <Link to="/career">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Carrer</NavLink>
              </NavItem>
            </Link>
            <Link to="/team">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Our Team</NavLink>
              </NavItem>
            </Link>
            <ScrollIntoView selector="#blog">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Blog</NavLink>
              </NavItem>
            </ScrollIntoView>
            <ScrollIntoView selector="#contact">
              <NavItem className="navLi">
                <NavLink className="color" href="javascript:void(0)">Contact Us</NavLink>
              </NavItem>
            </ScrollIntoView>
            <Link to="/hire">
              <NavItem className="navLi">
                <NavLink className="color button" href="javascript:void(0)">Hire</NavLink>
              </NavItem>
            </Link>
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  )
}

CommonNavbar.propTypes = {
  siteTitle: PropTypes.string,
}

CommonNavbar.defaultProps = {
  siteTitle: ``,
}

export default CommonNavbar;
