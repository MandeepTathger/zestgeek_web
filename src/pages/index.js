import React from "react"
import { Link } from "gatsby"
import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"
import Banner from "../components/Homepage/Banner"
import Ideas from "../components/Homepage/Ideas"
import Service from "../components/Homepage/services"
import Design from "../components/Homepage/design"
//import Testimonial from "../components/Homepage/testimonials"
import Contact from "../components/Homepage/contactus"
import Awards from "../components/Homepage/awards"
import Loadable from "react-loadable"


const loader=()=>(<div>Loading...</div>)

const Testimonial = Loadable({
  loader: () => import("../components/Homepage/testimonials"),
  loading: loader,
})

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Banner/>
    <Ideas/>
    <Service/>
    <Design/>
    <Testimonial/>
    <Contact/>
    <Awards/>
  </Layout>
)

export default IndexPage
