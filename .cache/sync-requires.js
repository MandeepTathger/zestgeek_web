const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---src-pages-404-js": hot(preferDefault(require("/Work/zestgeek_web/src/pages/404.js"))),
  "component---src-pages-career-js": hot(preferDefault(require("/Work/zestgeek_web/src/pages/career.js"))),
  "component---src-pages-hire-js": hot(preferDefault(require("/Work/zestgeek_web/src/pages/hire.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Work/zestgeek_web/src/pages/index.js"))),
  "component---src-pages-team-js": hot(preferDefault(require("/Work/zestgeek_web/src/pages/team.js")))
}

